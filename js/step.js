
const tabsTitleList = document.querySelector('.tabs');
tabsTitleList.addEventListener('click', (event) => {
    if (event.target.classList.contains('tabs-title')) {
        const attr = event.target.dataset.tab;
        document.querySelector('.tabs-title.active').classList.remove('active');
        event.target.classList.add('active');
        document.querySelector('.tab-content.active').classList.remove('active');
        document.querySelector(`[data-content=${attr}-content]`).classList.add('active');
    }
});


const tabsTitle = document.querySelectorAll(".section4-tabs-title");
const tabsContent = document.querySelectorAll(".section4-content");
const titleList = document.querySelector(".tabs-section4");
const buttonLoadMore = document.querySelector(".load-more");

let tabsContentArr = Array.from(tabsContent);
let i = 12;
buttonLoadMore.addEventListener("click",() => {
    if (i >= tabsContentArr.length - 12) {
        buttonLoadMore.remove();
    }
    i += 12;
    let tabsTitleActive = document.querySelector(".section4-tabs-title.active-title");
    tabsContentArr.forEach((img, index) => {
        if (index < i && (img.dataset.work === tabsTitleActive.dataset.title || tabsTitleActive.dataset.title === "all")) {
            img.classList.remove("hidden");
        } else {
            img.classList.add("hidden");
        }
    });
} );

titleList.addEventListener('click', function (event) {
    document.querySelector(".section4-tabs-title.active-title").classList.remove("active-title");
    event.target.classList.add("active-title");
    let chosenTitle = event.target.dataset["title"];
    tabsContentArr.forEach((elem, index) => {
        if (index < i) {
            elem.classList.remove('hidden');
            if (elem.dataset.work !== chosenTitle && chosenTitle !== 'all'){
                elem.classList.add('hidden');
            }
        }
    }
    )
});

new Swiper(".image-slider", {
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
  
    slidesPerView:1,
    
loop: true,
thumbs: {
    swiper: {
        el: ".image-mini-slider",
        slidesPerView: 4,
    },
    slideThumbActiveClass: "swiper-slide-thumb-active",
},  
});



